---
title: Firefox Send
icon: icon.svg
replaces: 
    - wetransfer
---

**Firefox Send** is an easy-to-use ephemeral end-to-end encrypted file sharing service from the makers of the independent [open source][floss] web browser.

You can sign in using a Firefox account to share larger files, for longer and to more people.

{{< infobox >}}
- **Website:**
    - [send.firefox.com](https://send.firefox.com/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}