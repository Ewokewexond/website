---
title: Whatsapp
subtitle: Instant Messengers
provider: facebook
order: 
    - signal
    - xmpp
    - quicksy
    - zom
    - riot
aliases:
    - /ethical-alternatives-to-whatsapp-and-skype/
---